import React from 'react';
import './scss/App.scss';

import logo from './images/logo.svg';

var week = ['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat'];

function App() {
  const [chart, setChart] = React.useState([]);
  const [currDay, setCurDay] = React.useState('');

  React.useEffect(() => {
    async function fetchData() {
      try {
        let res = await fetch('/data.json');
        let data = await res.json();

        setChart(data);
      } catch (error) {
        console.log(error);
      }
    }
    fetchData();
    const day = new Date().getDay();
    setCurDay(week[day]);
  }, []);

  return (
    <div className="wrapper">
      <div className="container">
        <header className="header">
          <div className="header-row">
            <div className="header-row__info">
              <span>My Balance</span>
              <h1>&#36;921.48</h1>
            </div>
            <div className="header-row__logo">
              <img src={logo} alt="" />
            </div>
          </div>
        </header>
        <main className="content">
          <div className="content-body">
            <p className="content-body__title">Spending - Last 7 days</p>
            <div className="content-body__chart">
              {chart.map((item, idx) => (
                <div
                  key={idx}
                  className={
                    item.day === currDay ? 'content-body__item active' : 'content-body__item'
                  }
                  data-day={item.day}
                  data-amount={item.amount}
                  style={{ height: item.amount * 3 }}></div>
              ))}
            </div>
          </div>
          <div className="content-footer footer">
            <div className="footer-row">
              <div className="footer-row__total">
                <span>Total this month</span>
                <p>&#36;478.33</p>
              </div>
              <div className="footer-row__month">
                <p>+2.4%</p>
                <span>from last month</span>
              </div>
            </div>
          </div>
        </main>
      </div>
    </div>
  );
}

export default App;
